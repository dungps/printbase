const shippingGroup = [
    {
        group: "Standard Shipping",
        shipping_rules: [
            {
                zone: "US",
                first_price: 8.99,
                add_price: 8.99,
            },
            {
                zone: "International",
                first_price: 6.99,
                add_price: 1.99
            }
        ]
    },
    {
        group: "Express Shipping",
        shipping_rules: [
            {
                zone: "US",
                first_price: 8.99,
                add_price: 8.99,
            },
            {
                zone: "International",
                first_price: 6.99,
                add_price: 1.99
            }
        ]
    },
    {
        group: "Premium Shipping",
        shipping_rules: [
            {
                zone: "US",
                first_price: 8.99,
                add_price: 8.99,
            },
            {
                zone: "International",
                first_price: 6.99,
                add_price: 1.99
            }
        ]
    }
]

module.exports = {
    shipping_zones: [
        {
            id: 1,
            zone: "US"
        },
        {
            id: 2,
            zone: "International"
        }
    ],
    list_badge: {
        list: []
    },
    list_category: [
        {
            id: 109,
            list_base_product: [
                {
                    "title": "Unisex T-shirt",
                    "short_description": "",
                    "product_description": "\u003cul\u003e\r\n\t\u003cli\u003eGildan brand\u003c/li\u003e\r\n\t\u003cli\u003eMade in USA\u003c/li\u003e\r\n\t\u003cli\u003e100% pre-shunk cotton\u003c/li\u003e\r\n\t\u003cli\u003eSeamless collar, taped neck and shoulders\u003c/li\u003e\r\n\t\u003cli\u003eDouble-needle sleeve and bottom hems\u003c/li\u003e\r\n\t\u003cli\u003eQuarter-turned to eliminate center crease\u003c/li\u003e\r\n\u003c/ul\u003e",
                    "group_display_name": "Gold Base",
                    "default_dimension": "",
                    "artworks": [
                        {
                            "template_url": "https://pb-stag.btdmp.com/pbase/templates/t-shirt.psd",
                            "part_name": "back",
                            "recommended_dimension": ""
                        },
                        {
                            "template_url": "",
                            "part_name": "front",
                            "recommended_dimension": ""
                        }
                    ],
                    "size_chart": {
                        "title": "dat-test",
                        "description_inc": "{}",
                        "description_html": "\u003cp\u003eabc\u003cimg alt=\"\" src=\"https://assets.boostflow.com/pod/print-files/fabric-facemask-1339894/15997372105f5a0d7ac1296.jpg\" /\u003e\u003c/p\u003e"
                    },
                    "size_data": [
                        {
                            "size": "S",
                            "price": 0.49,
                            "white_color_price": 0.49,
                            "other_color_price": 0.49,
                            "shipping_group": shippingGroup
                        },
                        {
                            "size": "M",
                            "price": 0.49,
                            "white_color_price": 0.49,
                            "other_color_price": 0.49,
                            "shipping_group": shippingGroup
                        },
                        {
                            "size": "L",
                            "price": 0.49,
                            "white_color_price": 0.49,
                            "other_color_price": 0.49,
                            "shipping_group": shippingGroup
                        },
                        {
                            "size": "XL",
                            "price": 0.49,
                            "white_color_price": 0.49,
                            "other_color_price": 0.49,
                            "shipping_group": shippingGroup
                        },
                        {
                            "size": "2XL",
                            "price": 1.99,
                            "white_color_price": 1.99,
                            "other_color_price": 1.99,
                            "shipping_group": shippingGroup
                        },
                        {
                            "size": "3XL",
                            "price": 3.49,
                            "white_color_price": 3.49,
                            "other_color_price": 3.49,
                            "shipping_group": shippingGroup
                        },
                        {
                            "size": "4XL",
                            "price": 3.99,
                            "white_color_price": 3.99,
                            "other_color_price": 3.99,
                            "shipping_group": shippingGroup
                        },
                        {
                            "size": "5XL",
                            "price": 3.99,
                            "white_color_price": 3.99,
                            "other_color_price": 3.99,
                            "shipping_group": shippingGroup
                        }
                    ],
                    "base_cost": 100,
                    "file_types_supported": [
                        "PNG"
                    ],
                    "minimum_dpi": 150,
                    "min_processing_day": 5,
                    "max_processing_day": 10,
                    "id": 3437,
                    "shipping_profile": [
                        {
                            "additional_item_price": 8.99,
                            "id": 1,
                            "shipping_fee": 8.99,
                            "shipping_time": "3",
                            "shipping_zone_id": 1
                        },
                        {
                            "additional_item_price": 1.99,
                            "id": 2,
                            "shipping_fee": 6.99,
                            "shipping_time": "3",
                            "shipping_zone_id": 2
                        }
                    ],
                    "image_catalog": "https://pb-stag.btdmp.com/phub/16075025695fd08ae9b8385.png",
                    "group_name": "Gold Base",
                    "selling_guide": [
                        {
                            "language": "zh",
                            "link": "https://www.shopbase.com/blog/zh/category/selling-guide/"
                        },
                        {
                            "language": "vi",
                            "link": "https://www.shopbase.com/blog/vn/category/selling-guide/"
                        },
                        {
                            "language": "en",
                            "link": "https://www.shopbase.com/blog/en/category/selling-guide/"
                        },
                        {
                            "language": "ab",
                            "link": "https://www.shopbase.com/blog/en/category/selling-guide/"
                        }
                    ]
                },
            ],
            name: "Apparel"
        }
    ]
}
