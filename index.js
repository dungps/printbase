const express = require("express")
const data = require("./shipping-data")
const { resolve } = require("path")
const app = express()

app.use("/assets", express.static(resolve(__dirname, "assets")))

app.get("/data", (req, res, next) => {
    res.setHeader("Content-Type", "application/json")
    res.sendFile(resolve(__dirname, "data.json"))
})

app.get("*", (req, res, next) => {
    res.sendFile(resolve(__dirname, "index.html"))
})

app.listen(3000)
